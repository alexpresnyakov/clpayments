USER = 'pk_ad838d8e62b89cafd6bc19d555679'
PASS = 'ae851e57426992c00ed5613db022159f'
TOKEN = '2F725BBD1F405A1ED0336ABAF85DDFEB6902A9984A76FD877C3B5CC3B5085A82'
ACCOUNT_ID = 1

RSpec.describe Clpayments do
  it "has a version number" do
    expect(Clpayments::VERSION).not_to be nil
  end

  it "check test" do
    pp Clpayments::Terminal.new(USER, PASS, debug: false).test
  end

  it "check test" do
    t = Clpayments::Terminal.new(USER, PASS, debug: false)
    pp t.create_order({
      amount: 100.0,
      currency: 'RUB',
      account_id: ACCOUNT_ID,
      invoice_id: '1345',
      description: 'Оплата в WaterApp'
    })
  end

  it "check auth payment" do
    return if TOKEN.blank?
    t = Clpayments::Terminal.new(USER, PASS, debug: false)
    pp t.token_auth({
      amount: 500.0,
      currency: 'RUB',
      account_id: ACCOUNT_ID,
      invoice_id: '1348',
      token: TOKEN,
      description: 'Оплата в WaterApp'
    })
  end


end
