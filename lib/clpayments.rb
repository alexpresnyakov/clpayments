require "clpayments/version"
require 'pp'
require 'http'
require 'active_support/all'

module Clpayments
  class Logger
    def initialize(opts)
      @opts = opts
    end

    def <<(it)
      return unless @opts[:debug]
      pp it
    end
  end

  class Api
    BASE_URL = 'https://api.cloudpayments.ru'

    def initialize(terminal, opts)
      @term = terminal
      @log = Logger.new(opts)
    end

    def post(name, params)
      url = [BASE_URL, '/', name].join
      @log << url

      @log << params
      res = HTTP.basic_auth(
        user: @term.user,
        pass: @term.password
        ).post(url, json: params)
      @log << res
      res.parse
    end
  end

  class Terminal
    STATUSES = ['AUTHORIZED',
                'CONFIRMED',
                'REVERSED',
                'REFUNDED',
                'PARTIAL_REFUNDED',
                'REJECTED']

    attr_accessor :user, :password

    def initialize(user, password, opts = {debug: true})
      @opts = opts
      @user = user
      @password = password

      @api = Api.new(self, @opts)
    end

    def test
      @api.post('test', {})
    end

    # h = {
    #   amount: 10.0,
    #   currency: 'RUB',
    #   require_confirmation: false,
    #   invoice_id: '111231asdf',
    #   account_id: '123123213',
    #   description: 'Назнвачение платежа'
    #
    # }
    def create_order(h)
      r = {
        Amount: h[:amount],
        Currency: h[:currency],
        RequireConfirmation: h[:require_confirmation] == true,
        InvoiceId: h[:invoice_id],
        AccountId: h[:account_id],
        Description: h[:description],
        onSuccess: 'https://yandex.ru',
        onFail: 'https://google.com'
      }

      if h[:json_data].present?
        r[:JsonData] = h[:json_data]
      end

      @api.post('orders/create', r)
    end

    def token_auth(h)
      r = {
        Amount: h[:amount],
        Currency: h[:currency],
        AccountId: h[:account_id],
        Token: h[:token],
        InvoiceId: h[:invoice_id],
        Description: h[:description]
      }
      @api.post('payments/tokens/auth', r)
    end

    def void(transaction_id)
      @api.post('payments/void', {
        TransactionId: transaction_id
      })
    end


  end
end
